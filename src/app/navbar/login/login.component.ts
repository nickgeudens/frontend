import { AccountService } from './../../shared/services/account.service';
import { Account } from './../../../models/account';
import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { HttpClient,HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username:string;
  password:string;
  
  constructor(private router: Router,private http:HttpClient,private account: AccountService) {

  }

  ngOnInit() {

  }
  loginUser(formdata){
      this.username = formdata.value.username;
      this.password = formdata.value.password;
      this.account.login(this.username,this.password);
  }

}
