import { element } from 'protractor';
import { Account } from './../../../models/account';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {


  changeUsernameModal: boolean =false;
  saved: boolean = false;
  account: Account = new Account;
  accountForm = new FormGroup({
    first_name: new FormControl(),
    last_name: new FormControl(),
    username: new FormControl()
  });

  constructor(private accountService:AccountService, private formBuilder: FormBuilder) {

  }
  ngOnInit() {

    this.accountService.getAccount().subscribe(data => {
      this.account = data;
      this.constructForm();
    });
  }
  constructForm(){
    this.accountForm = this.formBuilder.group({
      first_name:  [this.account.first_name],
      last_name: [this.account.last_name],
      username: [this.account.username, [Validators.required]]
    });
    this.accountForm.valueChanges.subscribe(account =>{
      this.submitValues();
    });
  }
  submitValues(){
    if(this.accountForm.status == "VALID"){
      this.account.first_name=this.accountForm.value.first_name;
      this.account.last_name=this.accountForm.value.last_name;
      this.account.username=this.accountForm.value.username;
      this.accountService.updateAccount(this.account).subscribe(
        ()=>this.saved=true,
        ()=>this.saved=false);
    }else{
      this.saved=false;
    }
  }

  urlChange(){
    this.accountService.changeProfilePicture();
  }
}

