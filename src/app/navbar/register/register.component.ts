import { AccountService } from './../../shared/services/account.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {NgForm, AbstractControl} from "@angular/forms/forms";
import {Router} from "@angular/router";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ValidatorFn } from '@angular/forms/src/directives/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
    username: new FormControl()
  });
  
  constructor(private http:HttpClient,
    private router: Router,
    public accountService: AccountService,
    private formBuilder: FormBuilder) {

    this.constructForm();
  }
  constructForm(){
    this.registerForm = this.formBuilder.group({
      email:  ["",[
        Validators.email,
        Validators.required
      ]],
      password: ["",[
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ]]
      });
      this.registerForm.get("email")
      this.registerForm.get("email").valueChanges.subscribe((email) =>{
      this.accountService.checkIfEmailExists(email);
      });
  }
  
  ngOnInit() {
  }
  
  registerUser() {
    if(this.registerForm.status == "VALID"){
    let email=this.registerForm.value.email;
    let password=this.registerForm.value.password;
    
    this.accountService.register(email,password)
    }
  }
}