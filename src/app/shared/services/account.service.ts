import { Account } from './../../../models/account';
import { Injectable, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {Router, ActivatedRoute} from "@angular/router";
import 'rxjs/Rx';
import { ValidatorFn, AbstractControl } from '@angular/forms';
@Injectable()
export class AccountService {

  profilePictureChanged = new EventEmitter();

  account: Account;
  public authenticated:boolean;
  public usernameExists:boolean;
  public emailExists:boolean;
  constructor(private http: HttpClient, private route: ActivatedRoute, private router:Router) {
  }

  getAccount() : Observable<Account> {
    if(this.authenticated){
      return this.http.get<Account>("/api/userdetails")
    }
    return Observable.empty<Account>();
  }

  updateAccount(account:Account){
    return this.http.put("/api/users/update",account)
  }

  login(emailOrUsername,password){
    const data = "username=" + encodeURIComponent(emailOrUsername) +
      "&password=" + encodeURIComponent(password) + "&submit=Login";
      const headers = new HttpHeaders ({
        "Respose-Type":"text",
      "Content-Type": "application/x-www-form-urlencoded"
    });
    this.http.post("/api/authentication",data, {headers}
    ).subscribe((resp: Response) => {
      console.log("you logged in as: " + emailOrUsername);
      this.checkAuthenticated();
      //this.route.snapshot.queryParams['target']
      this.router.navigate(['/home']);
    });
  }
  register(email,password){
    this.http.post('/api/users/create', {
      "email":email,
      "password":password
    }).subscribe(() => {
      console.log("you registered: "+email);
      this.login(email,password);
    });
  }
  logout(){
    this.http.post("/api/logout",{}).subscribe(data => {
      console.log("you logged out");
      this.router.navigate(['/home']);
    });
  }
  checkAuthenticated(){
    this.http.get("/api/authenticated").subscribe(s => this.authenticated= <boolean> s);
  }
  changeProfilePicture(){
    this.profilePictureChanged.emit();
  }
  checkIfUsernameExists(username:string){
    this.http.get("/api/users/username_exists?username="+encodeURIComponent(username)).subscribe(s => this.usernameExists= <boolean> s);
  }
  checkIfEmailExists(email:string){
    this.http.get("/api/users/email_exists?email="+ encodeURIComponent(email)).subscribe(s => this.emailExists= <boolean> s);
  }
}
