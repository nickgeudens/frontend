
import { Chuck } from './../../../models/chuck';
import { Card } from './../../../models/card';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders} from '@angular/common/http';
import { HttpBackend } from '@angular/common/http/src/backend';

@Injectable()
export class CardService {

  constructor(private http:HttpClient) { }

  getCards(): Observable<Card[]>{
    return this.http.get<Card[]>("/api/cards/all");
  }
  addCard(title:string,description:string){
    return this.http.post<Card>("/api/cards/add",{"title":title,"description":description});
  }
  deleteCard(card:Card){
    return this.http.delete("/api/cards/delete/"+card.id);
  }
  getRandomChuck() : Observable<Chuck> {
    return this.http.get<Chuck>("http://api.icndb.com/jokes/random?limitTo=[nerdy]");
  }
}
