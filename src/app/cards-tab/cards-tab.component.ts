import { Component, OnInit } from '@angular/core';
import { Card } from '../../models/card';
import { CardService } from '../shared/services/card.service';

@Component({
  selector: 'app-cards-tab',
  templateUrl: './cards-tab.component.html',
  styleUrls: ['./cards-tab.component.css']
})
export class CardsTabComponent implements OnInit {

  cards : Card[];
  
  constructor(private cardserv:CardService) { }

  ngOnInit() {
    this.getCards();
  }

  getCards(){
    this.cardserv.getCards().subscribe((cards: Card[])=>{
      this.cards=cards;
    });
  }
  update(){
    this.getCards();
  }

}
