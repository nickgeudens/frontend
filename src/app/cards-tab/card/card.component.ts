import { Card } from './../../../models/card';
import { CardService } from './../../shared/services/card.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnDestroy{

  subscription: Subscription;
  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  @Input() card: Card;

  @Output() updated = new EventEmitter<boolean>();
  
  editmode:boolean;

  constructor(private cardserv: CardService) { }

  ngOnInit() {
  }

  deleteCard(){
    this.subscription = this.cardserv.deleteCard(this.card).subscribe((card)=>{
      console.log("deleted "+this.card.id);
      this.updated.emit(true);
    });
  }

}
